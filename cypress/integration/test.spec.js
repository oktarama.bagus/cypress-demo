/* eslint-disable no-undef */
describe("Create a paragraph from form", () => {
  it("Displays the form", () => {
    cy.visit("http://localhost:3000");

    cy.get("form").should("have.id", "test");
  });

  it("Fill the form", () => {
    cy.get('[data-testid="name"]').type("Rama");
    cy.get('[data-testid="email"]').type("Rama@mail.com");
    cy.get('[data-testid="address"]').type("Rama Street");

    cy.get('[data-testid="sendButton"]').click();

    cy.get('[data-testid="name"]').should("have.value", "");
    cy.get('[data-testid="email"]').should("have.value", "");
    cy.get('[data-testid="address"]').should("have.value", "");
  });

  it("Show the paragraph", () => {
    cy.contains("Hi my name is Rama");
    cy.contains("i live in Rama Street");
    cy.contains("you can contact me via email at Rama@mail.com");
  });
});
