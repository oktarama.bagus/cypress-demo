import React, { useState } from "react";

const App = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [address, setAddress] = useState("");
  const [isSend, setIsSend] = useState(false);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };
  const handleNameEmailChange = (event) => {
    setEmail(event.target.value);
  };
  const handleAddressChange = (event) => {
    setAddress(event.target.value);
  };

  const handleSend = () => {
    document.getElementById("test").reset();
    setIsSend(true);
  };

  return (
    <div>
      <h1>Test Form Cypress</h1>

      <form id="test">
        <label>Name : </label>
        <input
          type="text"
          name="name"
          data-testid="name"
          onChange={handleNameChange}
        />
        <br />

        <label>Email : </label>
        <input
          type="text"
          name="email"
          data-testid="email"
          onChange={handleNameEmailChange}
        />
        <br />

        <label>Address : </label>
        <input
          type="text"
          name="address"
          data-testid="address"
          onChange={handleAddressChange}
        />
        <br />

        <button type="button" data-testid="sendButton" onClick={handleSend}>
          Send
        </button>
      </form>

      {isSend && (
        <>
          <p>Hi my name is {name}</p>
          <p>i live in {address}</p>
          <p>you can contact me via email at {email}</p>
        </>
      )}
    </div>
  );
};

export default App;
